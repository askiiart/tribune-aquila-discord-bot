# Tribune Aquila Discord bot

## Docker stuff

[![Build Status](https://drone.askiiart.net/api/badges/askiiart/tribune-aquila-discord-bot/status.svg)](https://drone.askiiart.net/askiiart/tribune-aquila-discord-bot)

A Tribune Pontius Aquila bot.

docker-compose.yml:

```yaml
services:
  aquila-bot:
    image: 'askiiart/discord-tribune-aquila-bot:latest'
    volumes:
      - ./.env:/app/.env
    restart: unless-stopped
```

.env:

```env
TOKEN='<YOUR_TOKEN_HERE>'
```

## What the bot actually does

When someone says "/aquila", the bot replies "Who gave permission for this? I sure as hell didn't."